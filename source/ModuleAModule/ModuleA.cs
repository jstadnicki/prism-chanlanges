﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using TradeCaptureApplication.Infrastructure;

namespace ModuleAModule
{
    public class ModuleA : IModule
    {
        private IUnityContainer container;
        private IRegionManager regionManager;

        public ModuleA(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.RegisterType<ToolbarView>();

            this.container.RegisterType<IContentAView, ContentView>();
            this.container.RegisterType<IContentViewAViewModel, ContentAViewModel>();

            // view discovery 
            this.regionManager.RegisterViewWithRegion(RegionNames.ToolbarRegion, typeof(ToolbarView));

            // view injection
            var vm = this.container.Resolve<IContentViewAViewModel>();
            var vm2 = this.container.Resolve<IContentViewAViewModel>(); 
            
            vm.Message = "Pierwszy view";
            vm2.Message = "Drugi view";

            var region = this.regionManager.Regions[RegionNames.ContentRegion];

            region.Add(vm.View);

            region.Deactivate(vm.View);
            region.Add(vm2.View);


        }
    }
}
