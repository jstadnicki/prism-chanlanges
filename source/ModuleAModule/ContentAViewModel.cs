﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;

namespace ModuleAModule
{
    public class ContentAViewModel : IContentViewAViewModel
    {

        public ContentAViewModel(IContentAView view)
        {
            this.View = view;
            this.View.ViewModel = this;
        }

        public IView View
        {
            get;
            set;
        }

        public string Message
        {
            get;

            set;

        }


        public string ViewName
        {
            get { return "ContentAViewModel"; }
        }
    }
}
