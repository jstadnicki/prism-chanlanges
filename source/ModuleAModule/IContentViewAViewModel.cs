﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;

namespace ModuleAModule
{
    public interface IContentViewAViewModel : IViewModel
    {
        string Message { get; set; }
    }
}
