﻿using System;

namespace TradeCaptureApplication.Infrastructure
{
    public interface IViewModel
    {
        IView View { get; set; }
        string ViewName { get; }
        
    }

}
