﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCapture.Business.Person;
using System.Collections.ObjectModel;

namespace TradeCaptureApplication.Infrastructure
{
    public interface IPersonRepository
    {
        int SavePerson(Person person);

        IEnumerable<Person> AllPeople { get; }
        void GetAllPeopleAsync(EventHandler<ServiceResult<IEnumerable<Person>>> callback);
    }
}
