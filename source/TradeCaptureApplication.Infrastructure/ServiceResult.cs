﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TradeCaptureApplication.Infrastructure
{
    public class  ServiceResult<T> : EventArgs
    {
        public T result;

        public ServiceResult(T p)
        {
            this.result = p;
        }
    }
}
