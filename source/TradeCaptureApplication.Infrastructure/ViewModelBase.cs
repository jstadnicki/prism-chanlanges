using System.ComponentModel;

namespace TradeCaptureApplication.Infrastructure
{
    public class ViewModelBase : IViewModel, INotifyPropertyChanged
    {
        public ViewModelBase(IView view)
        {
            this.View = view;
            this.View.ViewModel = this;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler onPropertyChanged = PropertyChanged;
            if (onPropertyChanged != null)
            {
                onPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public IView View { get; set; }


        virtual public string ViewName
        {
            get { return "ViewModel base"; }
        }
    }
}