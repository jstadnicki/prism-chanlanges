﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TradeCaptureApplication.Infrastructure
{
    public class RegionNames
    {
        public static string ToolbarRegion = "ToolbarResion";
        public static string StatusRegion = "StatusRegion";
        public static string ContentRegion = "ContentRegion";
        public static string DetailsRegion = "PersonDetailsRegion";
        
    }
}
