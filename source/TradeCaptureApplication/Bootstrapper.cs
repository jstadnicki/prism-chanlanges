﻿using System;
using System.Linq;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using System.Windows;

using ModuleAModule;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using System.Windows.Controls;
using TradeCapture.Module.People;
using TradeCaptureApplication.Infrastructure;
using StatusBar;
using ServicesModule;
using ViewA.Module;
using ViewB.Module;

namespace TradeCaptureApplication
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            Container.RegisterType<IShellViewModel, ShellViewModel>();
            return Container.Resolve<Shell>();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();


            App.Current.MainWindow = (Window)Shell;
            App.Current.MainWindow.Show();
        }

        protected override void ConfigureModuleCatalog()
        {
            //Type moduleAType = typeof(ModuleA);

            //ModuleCatalog.AddModule(new ModuleInfo()
            //{
            //    ModuleName = moduleAType.Name,
            //    ModuleType = moduleAType.AssemblyQualifiedName,
            //    InitializationMode = InitializationMode.WhenAvailable
            //});

            Type serviceModule = typeof(Services);

            ModuleCatalog.AddModule(new ModuleInfo()
            {
                ModuleName = serviceModule.Name,
                ModuleType = serviceModule.AssemblyQualifiedName,
                InitializationMode = InitializationMode.WhenAvailable
            });


            Type viewAmodule = typeof(ViewAModule);

            ModuleCatalog.AddModule(new ModuleInfo()
            {
                ModuleName = viewAmodule.Name,
                ModuleType = viewAmodule.AssemblyQualifiedName,
                InitializationMode = InitializationMode.WhenAvailable
            });


            Type viewBmodule = typeof(ViewBModule);

            ModuleCatalog.AddModule(new ModuleInfo()
            {
                ModuleName = viewBmodule.Name,
                ModuleType = viewBmodule.AssemblyQualifiedName,
                InitializationMode = InitializationMode.WhenAvailable
            });

            //Type peopleModule = typeof(PeopleModule);

            //ModuleCatalog.AddModule(new ModuleInfo()
            //{
            //    ModuleName = peopleModule.Name,
            //    ModuleType = peopleModule.AssemblyQualifiedName,
            //    InitializationMode = InitializationMode.WhenAvailable
            //});


            //Type personModule = typeof(PersonList.Module.PersonList);

            //ModuleCatalog.AddModule(new ModuleInfo()
            //{
            //    ModuleName = personModule.Name,
            //    ModuleType = personModule.AssemblyQualifiedName,
            //    InitializationMode = InitializationMode.WhenAvailable
            //});






            //Type toolbarModule = typeof(ToolbarModule.ToolbarModule);

            //ModuleCatalog.AddModule(new ModuleInfo()
            //{
            //    ModuleName = toolbarModule.Name,
            //    ModuleType = toolbarModule.AssemblyQualifiedName,
            //    InitializationMode = InitializationMode.WhenAvailable
            //});

            //Type statusModule = typeof(StatusBarModule);

            //ModuleCatalog.AddModule(new ModuleInfo()
            //{
            //    ModuleName = statusModule.Name,
            //    ModuleType = statusModule.AssemblyQualifiedName,
            //    InitializationMode = InitializationMode.WhenAvailable
            //});

            

            

        }

        protected override Microsoft.Practices.Prism.Regions.RegionAdapterMappings ConfigureRegionAdapterMappings()
        {
            RegionAdapterMappings mappings = base.ConfigureRegionAdapterMappings();
            mappings.RegisterMapping(typeof(StackPanel), Container.Resolve<StackPanelRegionAdapter>());
            return mappings;
        }
    }
}
