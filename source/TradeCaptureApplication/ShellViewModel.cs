﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.Commands;
using Xceed.Wpf.Toolkit;

namespace TradeCaptureApplication
{
    public class ShellViewModel : IShellViewModel
    {
        private IRegionManager regionManager;

        public ShellViewModel(IRegionManager regionManager)
        {
            this.regionManager = regionManager;
            NavigateCommand = new DelegateCommand<object>(Navigate);
            ApplicationCommands.NavigateCommand.RegisterCommand(this.NavigateCommand);
        }

        public DelegateCommand<object> NavigateCommand { get; private set; }

        private void Navigate(object navigatePath)
        {
            if (navigatePath != null)
            {
                this.regionManager.RequestNavigate(RegionNames.ContentRegion, navigatePath.ToString(), NavigationComplete);
            }
                
        }

        private void NavigationComplete(NavigationResult navResult)
        {
            //MessageBox.Show(String.Format("navigated to {0}", navResult.Context.Uri));
        }

    }
}
