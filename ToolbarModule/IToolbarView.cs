﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using TradeCaptureApplication.Infrastructure;

namespace ToolbarModule
{
    public interface IToolbarView: IView
    {
    }

    public interface IToolbarViewModel : IViewModel
    {
    }
}
