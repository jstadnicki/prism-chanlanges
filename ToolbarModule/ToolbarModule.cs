﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using TradeCaptureApplication.Infrastructure;

namespace ToolbarModule
{
    public class ToolbarModule : IModule
    {
        IUnityContainer container;
        IRegionManager regionManager;

        public ToolbarModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.RegisterType<IToolbarView, ToolbarView>();
            this.container.RegisterType<IToolbarViewModel, ToolbarViewModel>();

            var vm = this.container.Resolve<IToolbarViewModel>();
            this.regionManager.Regions[RegionNames.ToolbarRegion].Add(vm.View);


        }
    }
}
