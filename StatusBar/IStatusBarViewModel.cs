﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;

namespace StatusBar
{
    public interface IStatusBarViewModel : IViewModel
    {
        string Message { get; set; }
    }
}
