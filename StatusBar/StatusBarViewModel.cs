﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;
using Microsoft.Practices.Prism.Events;

namespace StatusBar
{
    public class StatusBarViewModel : ViewModelBase, IStatusBarViewModel
    {

        IEventAggregator eventAggregator;

        public StatusBarViewModel(IStatusBarView view, IEventAggregator eventAggregator) :
            base(view)
        {
            this.eventAggregator = eventAggregator;
            this.eventAggregator.GetEvent<PersonUpdatedEvents>().Subscribe(PersonUpdated);
        }

        string message;
        public string Message
        {
            get
            {
                return this.message;
            }
            set
            {
                this.message = value;
                this.OnPropertyChanged("Message");
            }
        }

        void PersonUpdated(string obj)
        {
            this.Message = obj;
        }
    }
}
