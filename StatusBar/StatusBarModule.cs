﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using TradeCaptureApplication.Infrastructure;

namespace StatusBar
{
    public class StatusBarModule : IModule
    {
        IUnityContainer container;
        IRegionManager regionManager;

        public StatusBarModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterTypesAndViews();

            var vm = this.container.Resolve<IStatusBarViewModel>();
            vm.Message = "Nowy mesedż";
            this.regionManager.Regions[RegionNames.StatusRegion].Add(vm.View);
        }

        private void RegisterTypesAndViews()
        {
            this.container.RegisterType<IStatusBarView, StatusBarView>();
            this.container.RegisterType<IStatusBarViewModel, StatusBarViewModel>();
        }
    }
}
