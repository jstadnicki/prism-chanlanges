﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;
using TradeCapture.Business.Person;
using System.Collections.ObjectModel;

namespace PersonList.Module
{
    public interface IPersonListVM : IViewModel
    {
        IEnumerable<Person> People { get; }
    }
}
