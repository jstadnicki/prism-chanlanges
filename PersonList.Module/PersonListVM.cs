﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using TradeCaptureApplication.Infrastructure;
using ServicesModule;
using System.Collections.ObjectModel;
using TradeCapture.Business.Person;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Xceed.Wpf.Toolkit;


namespace PersonList.Module
{
    public class PersonListVM : ViewModelBase, IPersonListVM
    {
        private IPersonRepository service;
        private IList<Person> people;
        private bool isBusy;
        private Person selectedPerson;
        private WindowState childWindowState;

        public DelegateCommand EditPersonDetails { get; set; }

        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }
            set
            {
                this.isBusy = value;
                OnPropertyChanged("IsBusy");
            }

        }

        public PersonListVM(IPersonListV view, IPersonRepository service)
            : base(view)
        {
            this.service = service;
            this.people = new List<Person>();

            this.IsBusy = true;
            this.service.GetAllPeopleAsync((s, e) =>
            {
                this.People = e.result;
                this.IsBusy = false;
            });

            this.EditPersonDetails = new DelegateCommand(EditPersonExecute, EditPersonCanExecute);
        }

        private void EditPersonExecute()
        {
            this.ChildWindowState = WindowState.Open;
        }

        public WindowState ChildWindowState
        {
            get
            {
                return this.childWindowState;
            }
            set
            {
                this.childWindowState = value;
                OnPropertyChanged("ChildWindowState");
            }
        }

        public Person SelectedPerson
        {
            get
            {
                return this.selectedPerson;
            }
            set
            {
                this.selectedPerson = value;
                this.EditPersonDetails.RaiseCanExecuteChanged();
                OnPropertyChanged("SelectedPerson");
            }

        }   

        private bool EditPersonCanExecute()
        {
            return (this.SelectedPerson != null);
                

        }



        public IEnumerable<Person> People
        {
            get { return this.people; }
            set
            {
                this.people = new List<Person>(value);
                OnPropertyChanged("People");
            }
        }
    }
}
