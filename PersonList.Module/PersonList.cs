﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using TradeCaptureApplication.Infrastructure;

namespace PersonList.Module
{
    public class PersonList : IModule
    {
        private IUnityContainer container;
        private IRegionManager regionManager;

        public PersonList(IUnityContainer container, IRegionManager regionmanager)
        {
            this.container = container;
            this.regionManager = regionmanager;
        }

        public void Initialize()
        {
            this.container.RegisterType<IPersonListVM, PersonListVM>();
            this.container.RegisterType<IPersonListV, PersonListView>();

            var vm = this.container.Resolve<IPersonListVM>();

            this.regionManager.Regions[RegionNames.ContentRegion].Add(vm.View);
        }
    }
}
