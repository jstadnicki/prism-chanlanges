﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace TradeCapture.Business.Person
{
    public class Person : INotifyPropertyChanged, IDataErrorInfo
    {
        private string firstName;

        private string lastName;

        private int age;

        private DateTime lastUpdated;

        private string error;

        private string mail;

        public override string ToString()
        {
            return string.Format("{0} {1}", this.FirstName, this.LastName);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler onPropertyChanged = PropertyChanged;
            if (onPropertyChanged != null)
            {
                onPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private string image;

        public string FirstName
        {
            get { return this.firstName; }
            set
            {
                this.firstName = value;
                this.OnPropertyChanged("FirstName");
            }

        }
        public string LastName
        {
            get { return this.lastName; }
            set
            {
                this.lastName = value;
                this.OnPropertyChanged("LastName");
            }
        }

        public int Age
        {
            get { return this.age; }
            set
            {
                this.age = value;
                this.OnPropertyChanged("Age");
            }
        }

        public DateTime LastUpdated
        {
            get { return this.lastUpdated; }
            set
            {
                this.lastUpdated = value;
                this.OnPropertyChanged("LastUpdated");
            }
        }

        public string Email
        {
            get
            {
                return this.mail;
            }
            set
            {
                this.mail = value;
                this.OnPropertyChanged("Email");
            }
        }

        public string Image
        {
            get { return this.image; }
            set
            {
                this.image = value;
                OnPropertyChanged("Image");
            }
        }

        public string Error
        {
            get { return this.error; }
            private set { this.error = value; }
        }

        public string this[string columnName]
        {
            get
            {
                Error = null;
                switch (columnName)
                {
                    case "FirstName":
                        if (string.IsNullOrWhiteSpace(this.FirstName))
                        {
                            Error = "First name is mandatory!";
                        }
                        break;
                    case "LastName":
                        if (string.IsNullOrWhiteSpace(this.LastName))
                        {
                            Error = "First name is mandatory!";
                        }
                        break;
                    case "Age":
                        if (this.Age < 0 || this.Age > 150)
                        {
                            Error = "It seems that age value is incorrent";
                        }
                        break;
                }
                return Error;
            }
        }
    }
}
