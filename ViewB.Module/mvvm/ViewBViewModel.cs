﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;

namespace ViewB.Module
{
    class ViewBViewModel : ViewModelBase, IViewBViewModel
    {
        public ViewBViewModel(IViewBView view) :
            base(view)
        {
        }

        public override string ViewName
        {
            get
            {
                return "ViewB ViewModel";
            }
        }

    }
}
