﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Microsoft.Practices.Prism.Regions;
using System.Windows;

namespace ViewB.Module.mvvm
{
    public interface IViewBContentViewModel
    {
    }

    public class ViewBContentViewModel : IViewBContentViewModel, INotifyPropertyChanged, INavigationAware, IConfirmNavigationRequest
    {
        int viewBShownCount;
        public int ViewBShownCount
        {
            get
            {
                return this.viewBShownCount;

            }
            set
            {
                this.viewBShownCount = value;
                this.OnPropertyChanged("ViewBShownCount");

            }
        }

        private void OnPropertyChanged(string p)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(p));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            this.ViewBShownCount++;
        }

        public void ConfirmNavigationRequest(NavigationContext navigationContext, Action<bool> continuationCallback)
        {
            if (MessageBox.Show("RU sure to leave this page?", "Q", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                continuationCallback(true);
                return;
            }
            continuationCallback(false);

        }
    }

}
