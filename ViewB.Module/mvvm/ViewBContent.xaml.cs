﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeCaptureApplication.Infrastructure;
using Microsoft.Practices.Prism.Regions;

namespace ViewB.Module.mvvm
{
    /// <summary>
    /// Interaction logic for ViewBContenet.xaml
    /// </summary>
    public partial class ViewBContent : UserControl, IViewBView, IRegionMemberLifetime
    {
        private IViewBContentViewModel vm;
        public ViewBContent(IViewBContentViewModel vm)
        {
            InitializeComponent();
            this.vm = vm;
            this.DataContext = vm;
            
        }

        public IViewModel ViewModel
        {
            get
            {
                return (IViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public bool KeepAlive
        {
            get { return false; }
        }
    }
}
