﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using TradeCaptureApplication.Infrastructure;
using ViewB.Module.mvvm;

namespace ViewB.Module
{
    public class ViewBModule : IModule
    {
        private IUnityContainer container;
        private IRegionManager regionManager;

        public ViewBModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }
        public void Initialize()
        {
            this.container.RegisterType<IViewBView, ViewBView>();
            this.container.RegisterType<IViewBViewModel, ViewBViewModel>();
            this.container.RegisterType<IViewBContentViewModel, ViewBContentViewModel>();
            this.container.RegisterType<object, ViewBContent>(typeof(ViewBContent).FullName);

            this.InitializeViews();
        }

        private void InitializeViews()
        {
            this.regionManager.RegisterViewWithRegion(RegionNames.ToolbarRegion, typeof(ViewBView));
            this.regionManager.RegisterViewWithRegion(RegionNames.ContentRegion, typeof(ViewBContent));
        }
    }
}
