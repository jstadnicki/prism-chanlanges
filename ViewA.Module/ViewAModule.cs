﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using TradeCaptureApplication.Infrastructure;
using ViewA.Module.mvvm;

namespace ViewA.Module
{
    public class ViewAModule : IModule
    {
        private IUnityContainer container;
        private IRegionManager regionManager;

        public ViewAModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.RegisterType<IViewAView, ViewAView>();
            this.container.RegisterType<IViewAViewModel, ViewAViewModel>();
            this.container.RegisterType<IViewAContentViewModel, ViewAContentViewModel>();
            this.container.RegisterType<IViewAEmailViewModel, ViewAEmailViewModel>();
            this.container.RegisterType<object, ViewAEmailView>(typeof(ViewAEmailView).FullName);


            this.container.RegisterType<object, ViewAContent>(typeof(ViewAContent).FullName);


            this.InitializeViews();
        }

        private void InitializeViews()
        {
            this.regionManager.RegisterViewWithRegion(RegionNames.ToolbarRegion, typeof(ViewAView));
            this.regionManager.RegisterViewWithRegion(RegionNames.ContentRegion, typeof(ViewAContent));
            //this.regionManager.RegisterViewWithRegion(RegionNames.ContentRegion, typeof(ViewAEmailView));
        }
    }
}
