﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;

namespace ViewA.Module.mvvm
{
    public class ViewAEmailViewModel : INotifyPropertyChanged, IViewAEmailViewModel, INavigationAware
    {
        IRegionNavigationJournal journal;

        public ViewAEmailViewModel()
        {
            this.CancelCommand = new DelegateCommand(CancelExecute);
        }

        private void CancelExecute()
        {
            this.journal.GoBack();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            var to = navigationContext.Parameters["to"];
            return to == this.Email;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            var query = navigationContext.Parameters["to"];
            this.Email = query;
            this.journal = navigationContext.NavigationService.Journal;
        }

        string email;
        public string Email
        {
            get
            {
                return this.email;
            }
            set
            {
                this.email = value;
                this.OnPropertyChanged("Email");
            }
        }


        string description;
        public string Description
        {
            get
            {
                return this.description;
            }
            set
            {
                this.description = value;
                this.OnPropertyChanged("Description");
            }
        }





        private void OnPropertyChanged(string p)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(p));

            }
        }


        public DelegateCommand CancelCommand { get; private set; }
    }

    public interface IViewAEmailViewModel
    {
    }
}
