﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;

namespace ViewA.Module
{
    public class ViewAViewModel : ViewModelBase, IViewAViewModel
    {
        public ViewAViewModel(IViewAView view) :
            base(view)
        {
        }

        public override string ViewName
        {
            get
            {
                return "ViewA ViewModel";
            }
        }

    }
}
