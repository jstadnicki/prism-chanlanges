﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeCaptureApplication.Infrastructure;

namespace ViewA.Module.mvvm
{
    /// <summary>
    /// Interaction logic for ViewANavigation.xaml
    /// </summary>
    public partial class ViewAContent : UserControl
    {
        private IViewAContentViewModel vm;

        public ViewAContent( IViewAContentViewModel vm )
        {
            InitializeComponent();
            this.vm = vm;
            this.DataContext = this.vm;
        }

        public TradeCaptureApplication.Infrastructure.IViewModel ViewModel
        {
            get
            {
                return (IViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
