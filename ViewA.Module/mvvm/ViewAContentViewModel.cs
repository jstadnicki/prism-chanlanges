﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using System.ComponentModel;
using TradeCapture.Business.Person;
using System.Collections.ObjectModel;
using TradeCaptureApplication.Infrastructure;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism;

namespace ViewA.Module.mvvm
{
    public interface IViewAContentViewModel
    {
    }

    public class ViewAContentViewModel : IViewAContentViewModel, INotifyPropertyChanged, INavigationAware
    {

        public ViewAContentViewModel(IPersonRepository repository, IRegionManager regionManager)
        {
            this.respository = repository;
            IsBusy = true;
            this.respository.GetAllPeopleAsync(HandleAllPeople);
            this.EmailCommand = new DelegateCommand(EmailExecute, EmailCanExecute);
            this.regionManager = regionManager;
        }

        int viewAContentShownCount;

        private void HandleAllPeople(object sender, ServiceResult<IEnumerable<Person>> e)
        {
            this.People = new ObservableCollection<Person>(e.result);
            this.IsBusy = false;

        }

        private bool EmailCanExecute()
        {
            return this.SelectedPerson != null;
        }

        private void EmailExecute()
        {
            var uriQuery = new UriQuery();
            uriQuery.Add("to", SelectedPerson.Email);
            var uri = new Uri(typeof(ViewAEmailView).FullName + uriQuery, UriKind.Relative);
            this.regionManager.RequestNavigate(RegionNames.ContentRegion, uri);
        }

        bool isBusy;
        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }
            set
            {
                this.isBusy = value;
                this.OnPropertyChanged("IsBusy");
            }

        }

        public int ViewAContentShownCount
        {
            get
            {
                return this.viewAContentShownCount;
            }
            set
            {
                this.viewAContentShownCount = value;
                this.OnPropertyChanged("ViewAContentShownCount");
            }
        }

        Person selectedPerson;
        public Person SelectedPerson
        {
            get
            {
                return this.selectedPerson;
            }
            set
            {
                this.selectedPerson = value;
                this.EmailCommand.RaiseCanExecuteChanged();
                OnPropertyChanged("SelectedPerson");
            }
        }

        ObservableCollection<Person> people;
        public ObservableCollection<Person> People
        {
            get
            {
                return this.people;
            }
            set
            {
                this.people = value;
                OnPropertyChanged("People");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private IPersonRepository respository;
        private IRegionManager regionManager;

        private void OnPropertyChanged(string propname)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propname));
            }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            this.ViewAContentShownCount++;
        }

        public DelegateCommand EmailCommand { get; private set; }
    }
}
