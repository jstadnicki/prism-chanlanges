﻿using System;
using System.Linq;
using TradeCaptureApplication.Infrastructure;

namespace TradeCapture.Module.People
{
    public interface IPersonView : IView
    {
        IPersonViewModel PersonViewModel { get; set; }
    }
}
