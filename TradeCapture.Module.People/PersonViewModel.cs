﻿using System;
using System.Linq;
using Microsoft.Practices.Prism.Commands;
using TradeCapture.Business.Person;
using TradeCaptureApplication.Infrastructure;
using Microsoft.Practices.Prism.Events;
using System.Windows;

namespace TradeCapture.Module.People
{
    class PersonViewModel : ViewModelBase, IPersonViewModel
    {
        IEventAggregator eventAggregator;
        IPersonRepository personRepository;
        public PersonViewModel(IPersonView view, IEventAggregator eventAggregator, IPersonRepository personRepository)
            : base(view)
        {
            view.PersonViewModel = this;
            this.personRepository = personRepository;
            this.eventAggregator = eventAggregator;
            this.CreatePerson("joe", "doe", 33);
            this.SaveCommand = new DelegateCommand(Save, CanSave);
            GlobalCommands.SaveAllCommand.RegisterCommand(SaveCommand);
        }

        override public string ViewName
        {
            get { return this.Person.FirstName; }
        }

        public DelegateCommand SaveCommand { get; private set; }

        private Person person;

        private void Save()
        {
            int c = this.personRepository.SavePerson(this.Person);
            this.eventAggregator.GetEvent<PersonUpdatedEvents>().
                Publish(string.Format("{1}, {0} was updated", this.Person.FirstName, this.Person.LastName));
        }

        private bool CanSave()
        {
            return this.Person.Error == null;
        }

        public Person Person
        {
            get { return this.person; }
            set
            {
                this.person = value;
                this.person.PropertyChanged += person_PropertyChanged;
                this.OnPropertyChanged("Person");
            }
        }

        void person_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            this.SaveCommand.RaiseCanExecuteChanged();
        }

        public void CreatePerson(string fname, string lname, int age)
        {
            this.Person = new Person()
            {
                Age = age,
                FirstName = fname,
                LastName = lname
            };
        }
    }

}
