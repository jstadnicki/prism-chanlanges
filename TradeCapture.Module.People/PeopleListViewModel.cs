﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;
using TradeCapture.Business.Person;
using System.Collections.ObjectModel;

namespace TradeCapture.Module.People
{
    public class PeopleListViewModel : ViewModelBase, IPeopleListViewModel
    {
        private ObservableCollection<Person> people;
        public PeopleListViewModel(IPeopleListView view) :
            base(view)
        {
            this.people = new ObservableCollection<Person>();
            this.PopulatePeopleList();
        }

        override public string ViewName
        {
            get { return "List of all people"; }
        }

        private void PopulatePeopleList()
        {
            for (int i = 0; i < 22; i++)
            {
                this.people.Add(new Person()
                {
                    Age = 0,
                    FirstName = string.Format("First {0}", i),
                    LastName = string.Format("Last {0}", i),
                });
            }
        }

        public ObservableCollection<Person> People
        {
            get
            {
                return this.people;
            }
        }
    }
}
