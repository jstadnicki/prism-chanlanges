﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeCaptureApplication.Infrastructure;

namespace TradeCapture.Module.People
{
    /// <summary>
    /// Interaction logic for PeopleList.xaml
    /// </summary>
    public partial class PeopleList : UserControl, IPeopleListView
    {
        public PeopleList()
        {
            InitializeComponent();
        }

        public IViewModel ViewModel
        {
            get
            {
                return (IViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
