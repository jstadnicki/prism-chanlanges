﻿using System;
using System.Linq;
using System.Windows.Controls;
using TradeCaptureApplication.Infrastructure;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism;
using TradeCapture.Business.Person;

namespace TradeCapture.Module.People
{
    /// <summary>
    /// Interaction logic for PersonView.xaml
    /// </summary>
    public partial class PersonView : UserControl, IPersonView
    {
        public PersonView()
        {
            InitializeComponent();
            RegionContext.GetObservableContext(this).PropertyChanged += (s, e) =>
                {
                    var context = (ObservableObject<object>)s;
                    var selectedp = (Person)context.Value;
                    this.PersonViewModel.Person= selectedp;
                };

        }

        public IViewModel ViewModel
        {
            get
            {
                return (IViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public IPersonViewModel PersonViewModel
        {
            get;
            set;
        }
    }
}
