﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using TradeCaptureApplication.Infrastructure;

namespace TradeCapture.Module.People
{
    public class PeopleModule : IModule
    {
        private readonly IUnityContainer container;

        private readonly IRegionManager regionManager;

        public PeopleModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.regionManager = regionManager;
            this.container = container;
        }

        public void Initialize()
        {
            RegisterViewsAndServices();

            //var vm1 = this.container.Resolve<IPersonViewModel>();
            //vm1.CreatePerson("jan", "bean", 55);
            //this.regionManager.Regions[RegionNames.ContentRegion].Add(vm1.View);

            //var vm2 = this.container.Resolve<IPersonViewModel>();
            //vm2.CreatePerson("elizabeth", "krolowa", 78);
            //this.regionManager.Regions[RegionNames.ContentRegion].Add(vm2.View);

            //var vm3 = this.container.Resolve<IPersonViewModel>();
            //vm3.CreatePerson("roki", "balboa", 23);
            //this.regionManager.Regions[RegionNames.ContentRegion].Add(vm3.View);

            var listvm1 = this.container.Resolve<IPeopleListViewModel>();
            this.regionManager.Regions[RegionNames.ContentRegion].Add(listvm1.View);

            var detailsvm = this.container.Resolve<IPersonViewModel>();
            this.regionManager.Regions[RegionNames.DetailsRegion].Add(detailsvm.View);

            //var listvm2 = this.container.Resolve<IPeopleListViewModel>();
            //this.regionManager.Regions[RegionNames.ContentRegion].Add(listvm2.View);
        }

        private void RegisterViewsAndServices()
        {
            this.container.RegisterType<IPersonView, PersonView>();
            this.container.RegisterType<IPersonViewModel, PersonViewModel>();
            this.container.RegisterType<IPeopleListView, PeopleList>();
            this.container.RegisterType<IPeopleListViewModel, PeopleListViewModel>();

            
        }
    }
}
