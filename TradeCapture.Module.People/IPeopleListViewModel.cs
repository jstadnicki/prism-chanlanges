﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;
using TradeCapture.Business.Person;
using System.Collections.ObjectModel;

namespace TradeCapture.Module.People
{
    public interface IPeopleListViewModel : IViewModel
    {
        ObservableCollection<Person> People { get; }
    }
}
