﻿using System;
using TradeCapture.Business.Person;
using TradeCaptureApplication.Infrastructure;

namespace TradeCapture.Module.People
{
    public interface IPersonViewModel : IViewModel
    {
        Person Person { get; set; }
        void CreatePerson(string fname, string lname, int age);
    }
}
