﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TradeCaptureApplication.Infrastructure;
using TradeCapture.Business.Person;
using System.Collections.ObjectModel;
using System.Threading;
using System.ComponentModel;

namespace ServicesModule
{
    public class PersonRepository : IPersonRepository
    {
        int count = 0;

        public int SavePerson(Person person)
        {
            this.count++;
            person.LastUpdated = DateTime.Now;
            return count;
        }

        public IEnumerable<Person> AllPeople
        {
            get
            {
                List<Person> result = new List<Person>();
                for (int i = 0; i < 15; i++)
                {
                    result.Add(new Person
                    {
                        Age = 18 + i,
                        FirstName = "John" + i,
                        LastName = "Doe" + i,
                        Image = "/Icon" + DateTime.Now.Millisecond % 2 + ".bmp",
                    });
                    var r = result.ElementAt(i);
                    r.Email = r.FirstName + r.LastName + "@mail.com";
                    Thread.Sleep(175);
                }
                return result;
            }
        }

        public void GetAllPeopleAsync(EventHandler<ServiceResult<IEnumerable<Person>>> callback)
        {
            BackgroundWorker bw = new BackgroundWorker();

            bw.DoWork += (s, e) =>
                {
                    e.Result = this.AllPeople;
                };

            bw.RunWorkerCompleted += (s, e) =>
                {
                    callback.Invoke(this, new ServiceResult<IEnumerable<Person>>((IEnumerable<Person>)e.Result));
                };

            bw.RunWorkerAsync();
        }
    }
}
